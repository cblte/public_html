# public_html

To generate this page, download [mkws](https://mkws.sh/) and [smu](https://github.com/Gottox/smu) a simple markdown converter.

Unpack the binary of `mkws` and `smu` into `/bin` folder. The simple markdown processor `smu` can just be compiled here on envs by executing the `make` in the folder where you unpacked the `smu`archive.

When all binaries are in place, run the `./generate.sh` script.

This website also makes use of the [water.css](https://github.com/kognise/water.css) stylesheet.
